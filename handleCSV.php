<?php
    
    require_once('entityees/Basket.php');
    require_once('entityees/Book.php');
    require_once('entityees/ExclusiveBook.php');
    require_once('entityees/NewBook.php');
    require_once('entityees/UsedBook.php');
    require_once('PrintBook.php');

    $fileCSV = fopen('csv_books.csv', 'r');
    $headers = fgets($fileCSV);
    while(!feof($fileCSV)){
        $line = fgets($fileCSV);
        $data = explode(',', $line);
        if($data[0] == 'ExclusiveBook'){
            $book = new ExclusiveBook();
        }else if($data[0] == 'NewBook'){
            $book = new NewBook();
        }else{
            $book = new UsedBook(); 
        }
        $book->setTitle($data[1]);
        $book->setISBN($data[2]);
        $book->setPrice($data[3]);
        $book->setAut(str_replace('|', ', ', $data[4]));
        Basket::addBook($book);
    }
    fclose($fileCSV);
    PrintBook::printBooks(Basket::$books);
?>

