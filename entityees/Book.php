<?php

    abstract class Book{

        abstract public function getPrice();

        protected $title;
        protected $aut;
        protected $ISBN = '';
        protected $price;
        protected $type;

        function __construct($type){
            $randomString = chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90));
            $this->type = $type;
            $this->ISBN = $randomString;
        }

        public function getTitle(){
            return $this->title;
        }

        public function getAut(){
            return $this->aut;
        }

        public function getISBN(){
            return $this->ISBN;
        }

        public function getType(){
            return $this->type;
        }

        public function setTitle($title){
            $this->title = $title;
        }

        public function setAut($aut){
            $this->aut = $aut;
        }

        public function setISBN($ISBN){
            $this->ISBN = $ISBN;
        }

        public function setPrice($price){
            $this->price = $price;
        }
    }
    
?>