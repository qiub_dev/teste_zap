<?php
    require_once('Book.php');
    class UsedBook extends Book{

        function __construct(){
            parent::__construct('UsedBook');
        }

        public function getPrice(){
            $discPrice = $this->price - (0.25)*($this->price);
            return $discPrice;
        }
    }
    
?>