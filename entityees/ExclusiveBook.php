<?php
    require_once('Book.php');
    class ExclusiveBook extends Book{

        function __construct(){
            parent::__construct('ExclusiveBook');
        }

        public function getPrice(){
            $discPrice = $this->price;
            return $discPrice;
        }
    }    

?>