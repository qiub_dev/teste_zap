<?php

    class Basket{

        static $books = Array();
        static $totalBooks = 0;
        static $totalPrice = 0.0;

        static public function getTotalBooks(){
            return self::totalBooks;
        }

        static public function getTotalPrice(){
            return self::totalPrice;
        }

        static public function addBook($book){
            self::$books[] = $book;
            self::$totalBooks += 1;
            self::$totalPrice += $book->getPrice();
        }

    }

?>