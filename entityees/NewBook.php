<?php
   require_once('Book.php');
   class NewBook extends Book{

        function __construct(){
            parent::__construct('NewBook');
        }

        public function getPrice(){
            $discPrice = $this->price - (0.1)*($this->price);
            return $discPrice;
        }
        
    }  
    
?>