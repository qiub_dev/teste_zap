#EXERC�CIO BOOKSTORE

Este exerc�cio basea-se na implementa��o de uma aplica��o php, que exemplifica a utiliza��o de programa��o orientada a objectos e conceitos de DRY a fim de simular uma aplica��o de venda de livros (Simples)

---------------------------------------

##Tecnologias a utilizar

** Podem ser usadas quaisquer outras tecnologias alternativas, com base no seu sistema operacional. **

+ Baixar e instalar o xampp (https://www.apachefriends.org/download.html)
+ Baixar e instalar o editor de texto Visual Studio Code (https://code.visualstudio.com/download)
* Baixar e instalar o Git (https://git-scm.com/downloads)

---------------------------------------

Com as tecnologias acima ou semlhantes instaladas, podemos dar inicio.

##Preparando o ambiente
* Criar uma pasta em xampp/htdocs para receber o projecto
* Abrir a mesma no cmd ou em algum editor de texto com recurso a um terminal
* Clonar o projecto no reposit�rio p�blico: ``` git clone https://qiub_dev@bitbucket.org/qiub_dev/teste_zap.git ```

  Ap�s clonar o projecto, teremos em nossa pasta uma aplica��o l�ravel com os arquivos necess�rios para a execu��o do script.


* Iniciar o servidor de aplica��o para rodar o script php
	* Abrir o xampp, de seguida iniciar o servidor de aplica��o ** na porta 80 **
		
#Executando o scrept.

Pode-se rodar o programa atravez de dois endere�os:


* http://localhost/TesteZap/teste_zap/ - Neste caso ** TesteZap ** � a pasta criada em xampp/htdocs e ** teste_zap ** � o nome do projecto vindo do reposit�rio.
	Ao buscar por esse endere�o o scrip carrega alguns objectos em memoria, e mostra os resultados com base no formato pedido no exerc�cio prop�sto. Abaixo temos uma ilustra��o do carregamento dos objectos em mem�ria:
	
	
	```
		
		$usedBook = new UsedBook();
		$exBook = new ExclusiveBook();
		$newBook = new NewBook();

		$exBook->setTitle('Livro de programa��o em c/c++ fundamental');
		$exBook->setAut('Justino Sachilombo');
		$exBook->setPrice(1000.555);

		$usedBook->setTitle('Livro de matem�tica fundamental');
		$usedBook->setAut('Justino Sachilombo, Maria da Costa');
		$usedBook->setPrice(1000.555);

		$newBook->setTitle('Livro de f�sica fundamental');
		$newBook->setAut('Justino Sachilombo');
		$newBook->setPrice(1000.555);

		Basket::addBook($exBook);
		Basket::addBook($newBook);    
		Basket::addBook($usedBook);    

		PrintBook::printBooks(Basket::$books);   
	```


* http://localhost/TesteZap/teste_zap/handleCSV.php - Novamente ** TesteZap ** � a pasta criada em xampp/htdocs, ** teste_zap ** � o nome do projecto vindo do reposit�rio e ** handleCSV.php ** � o nome do arquivo que cont�m o script para importar e mostras os livros vindo de um CSV.
	Ao buscar por esse endere�o o scrip abre o CSV para cada linha cria o respectivo objecto Livro, com base no ** tipo **, e mostra os resultados conforme o formato pedido no exerc�cio prop�sto. Abaixo temos uma ilustra��o basica do script:
	
	
	```
		
		<?php
    
			require_once('entityees/Basket.php');
			require_once('entityees/Book.php');
			require_once('entityees/ExclusiveBook.php');
			require_once('entityees/NewBook.php');
			require_once('entityees/UsedBook.php');
			require_once('PrintBook.php');

			$fileCSV = fopen('csv_books.csv', 'r');
			$headers = fgets($fileCSV);
			while(!feof($fileCSV)){
				$line = fgets($fileCSV);
				$data = explode(',', $line);
				if($data[0] == 'ExclusiveBook'){
					$book = new ExclusiveBook();
				}else if($data[0] == 'NewBook'){
					$book = new NewBook();
				}else{
					$book = new UsedBook(); 
				}
				$book->setTitle($data[1]);
				$book->setISBN($data[2]);
				$book->setPrice($data[3]);
				$book->setAut(str_replace('|', ', ', $data[4]));
				Basket::addBook($book);
			}
			fclose($fileCSV);
			PrintBook::printBooks(Basket::$books);
		?>
	```


## Esta foi mais uma apresenta��o simples de como funciona o exerc�cio BookStore. no endere�o (https://qiub_dev@bitbucket.org/qiub_dev/teste_zap_bonus.git) cont�m um reposit�rio com uma apica��o Laravel (API), que fornece mais recursos para o exerc�cio a cima.

OBRIGADO


