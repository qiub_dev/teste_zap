<?php

class PrintBook{

    static public function printBooks($books){

        echo '------------------------------------------------------BASKET-------------------------------------------------------'. '</br></br>';

            echo 'Total de livros: '.Basket::$totalBooks. '</br>';
            echo 'Preço a pagar: '.number_format(Basket::$totalPrice, 2). '</br></br>';

        echo '-------------------------------------------------------BOOKS---------------------------------------------------------'. '</br></br>';
        
        foreach ($books as $value) {
            echo '€ '.number_format($value->getPrice(), 2).' '.$value->getType().' '.$value->getISBN().': '.$value->getTitle().' - '.$value->getAut(). '</br></br>';
        }
        echo '€ '.number_format(Basket::$totalPrice, 2).' - Total';
    }
} 
    
?>