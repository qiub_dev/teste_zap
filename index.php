<?php

    require_once('entityees/Basket.php');
    require_once('entityees/Book.php');
    require_once('entityees/ExclusiveBook.php');
    require_once('entityees/NewBook.php');
    require_once('entityees/UsedBook.php');
    require_once('PrintBook.php');

    $usedBook = new UsedBook();
    $exBook = new ExclusiveBook();
    $newBook = new NewBook();

    $exBook->setTitle('Livro de programação em c/c++ fundamental');
    $exBook->setAut('Justino Sachilombo');
    $exBook->setPrice(1000.555);

    $usedBook->setTitle('Livro de matemática fundamental');
    $usedBook->setAut('Justino Sachilombo, Maria da Costa');
    $usedBook->setPrice(1000.555);

    $newBook->setTitle('Livro de física fundamental');
    $newBook->setAut('Justino Sachilombo');
    $newBook->setPrice(1000.555);

    Basket::addBook($exBook);
    Basket::addBook($newBook);    
    Basket::addBook($usedBook);    

    PrintBook::printBooks(Basket::$books);   
    
?>